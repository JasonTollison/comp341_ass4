import time
import re

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)
pattern = re.compile("^[\\d]+$") #Regex looking for a digit 1 to many digits long

#Saves number of times user has seen a page to the redis database
def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

#Checks if a numer is prime. Returns boolean
def isPrime(n):
 if n==2 or n==3: return True
 if n%2==0 or n<2: return False
 for i in range(3,int(n**0.5)+1,2):   # only odd numbers
  if n%i==0:
   return False    

 return True

#Displays a hello message and the number of times the page has been seen to the user
@app.route('/')
def hello():
    count = get_hit_count()
    return 'Guten tag welt! I have been seen {} times.\n'.format(count)

#Displays error message when user doesn't privde any input
@app.route('/isPrime/')
def noInput():
	return 'You need to enter an integer to test'

#Tests if an integer is prime. If it's prime it is stored as a set in redis database.
#Result returned to user, if input from user is not an integer error message returned
@app.route('/isPrime/<n>', methods=["GET"])
def checkNumber(n):
	if pattern.match(n):
		i = int(n)
		if isPrime(i):
			if (cache.sismember('primes', n) == 0):
				cache.sadd('primes', n)
			return str(n) + " is prime"
		return str(n) + " is not prime"
	return "That is not an integer"

#Returns a set of all stored prime numbers from redis database
@app.route('/primesStored')
def displayPrimes():
	liLength = cache.scard('primes')
	if liLength > 0:
		primeString = ""
		primeSet = cache.smembers('primes')
		for x in primeSet:
			digit = x.decode('ascii')
			primeString += str(digit) + " "
		return primeString
	return "No prime numbers stored in redis"
